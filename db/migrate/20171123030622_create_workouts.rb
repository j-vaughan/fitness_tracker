class CreateWorkouts < ActiveRecord::Migration[5.1]
  def change
    create_table :workouts do |t|
      t.datetime 	:day
      t.text 			:description
      t.float     :weight

      t.timestamps
    end
  end
end
